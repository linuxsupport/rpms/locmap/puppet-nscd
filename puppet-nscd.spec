Name:           puppet-nscd
Version:        2.3
Release:        2%{?dist}
Summary:        Puppet module for nscd service

Group:          CERN/Utilities
License:        BSD
URL:            http://linux.cern.ch
Source0:        %{name}-%{version}.tgz

BuildArch:      noarch

Requires:       puppet-agent

%description
Puppet module for nscd service.

%prep
%setup -q

%build
CFLAGS="%{optflags}"

%install
rm -rf %{buildroot}
install -d %{buildroot}/%{_datadir}/puppet/modules/nscd/
cp -ra code/* %{buildroot}/%{_datadir}/puppet/modules/nscd/
touch %{buildroot}/%{_datadir}/puppet/modules/nscd/linuxsupport

%files -n puppet-nscd
%{_datadir}/puppet/modules/nscd
%doc code/README.md

%changelog
* Fri Dec 02 2022 Ben Morrice <ben.morrice@cern.ch> 2.3-2
- Bump release for disttag change

* Tue Mar 29 2022 Ben Morrice <ben.morrice@cern.ch> - 2.3-1
- update nscd ttl to CS recommendations (https://service-dns.web.cern.ch/faq.asp)

* Mon Nov 22 2021 Ben Morrice <ben.morrice@cern.ch> - 2.2-1
- force provider to be systemd

* Tue Feb 23 2021 Ben Morrice <ben.morrice@cern.ch> - 2.1-2
- correct requires to ensure correct package removal

* Thu Feb 11 2021 Ben Morrice <ben.morrice@cern.ch> - 2.1-1
- move away from hiera lookups

* Thu Jan 09 2020 Ben Morrice <ben.morrice@cern.ch> - 2.0-2
 - Rebuild for el8

* Thu May 03 2018 Thomas Oulevey <thomas.oulevey@cern.ch> - 2.0-1
- Rebuild for 7.5

* Tue Nov 29 2016 Thomas Oulevey <thomas.oulevey@cern.ch> - 0.2-1
- Rebuild for 7.3 release

* Fri Jun 17 2016 Aris Boutselis <aris.boutselis@cern.ch>
-Initial release
