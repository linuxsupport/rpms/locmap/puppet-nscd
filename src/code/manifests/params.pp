#
# == Class nscd::params
# Declare the default parameters
#
class nscd::params {

  $nscd_hosts_cache      = lookup({"name" => "nscd_hosts_cache", "default_value" => 'yes'})
  $nscd_passwd_cache     = lookup({"name" => "nscd_passwd_cache", "default_value" => 'no'})
  $nscd_group_cache      = lookup({"name" => "nscd_group_cache", "default_value" => 'no'})
  $nscd_paranoia         = lookup({"name" => "nscd_paranoia", "default_value" => 'yes'})
  $nscd_restart_interval = '3600'
        $owner                 = 'root'
        $group                 = 'root'
        $mode                  = '0644'

}
