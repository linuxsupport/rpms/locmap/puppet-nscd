# == Class: nscd
#
# Manages /etc/nscd.conf file and service.
#
# === Parameters
# The nscd::config class takes variables from hiera.

# === Variables
#
# === Examples
#
#  class { nscd: }
#
#
class nscd {

 class {'nscd::params':}
 class {'nscd::install':}
 class {'nscd::config':}
 class {'nscd::service':}

}
