#
# == Class nscd::config
# Configures nscd:
#
# === Parameters
# [*nscd_hosts_cache*]
# Set to yes or no, should hosts be cached or not by nscd.
# Default: yes
#
# [*nscd_group_cache*]
# Set to yes or no, should hosts be cached or not by nscd.
# Default: no
# 
# [*nscd_passwd_cache*]
# Set to yes or no, should hosts be cached or not by nscd.
# Default: no
class nscd::config {


     if ! ( $nscd::params::nscd_hosts_cache in ['yes','no'] ) {
        fail('nscd_hosts_cache must be yes or no')
     }
     if ! ( $nscd::params::nscd_passwd_cache in ['yes','no'] ) {
        fail('nscd_hosts_cache must be yes or no')
     }
     if ! ( $nscd::params::nscd_group_cache in ['yes','no'] ) {
        fail('nscd_hosts_cache must be yes or no')
     }
     if ! ( $nscd::params::nscd_paranoia in ['yes','no'] ) {
        fail('nscd_paranoia must be yes or no')
     }


     file{'/etc/nscd.conf':
        ensure  => present,
        content => template('nscd/nscd.conf.erb'),
        require => Package['nscd'],
        notify  => Service['nscd'],
        mode    => $nscd::params::mode,
        owner   => $nscd::params::owner,
        group   => $nscd::params::group,
     }
}

